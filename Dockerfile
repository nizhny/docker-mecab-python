FROM python:3
WORKDIR ~


RUN apt-get update && apt-get install -y \
    bzip2 \
    git \
    htop \
    sudo \
    wget \
		locales

RUN localedef -f UTF-8 -i ja_JP ja_JP.UTF-8

ENV LANG=ja_JP.UTF-8
ENV LANGUAGE=ja_JP:ja
ENV LC_ALL=ja_JP.UTF-8

RUN apt-get install -y  mecab libmecab-dev mecab-ipadic-utf8 git make curl xz-utils file
RUN git clone --depth 1 https://github.com/neologd/mecab-ipadic-neologd.git && \
    cd mecab-ipadic-neologd && \
    bin/install-mecab-ipadic-neologd -n -y 


RUN pip install --upgrade pip && \
    pip install mecab-python3== 0.996.5

COPY ./mecabrc /etc/mecabrc
